import { Scale } from 'tonal'

const app = new Vue({
  el: '#song-table',
  // methods: {
  //   commonChords: function (key) {
  //     // const tonalKey = key.endsWith('m') ? key.slice(0, key.length - 1) + 'minor' : key + 'major'
  //     return Scale.toScale(key + ' major')
  //   }
  // },
  data: { songs: [
    {
      name: "That's life!",
      youtube: {
        description: 'Sinatra',
        link: 'https://youtu.be/avU2aarQUiU'
      },
      key: 'G',
      source: 'iReal Pro'
    },
    {
      name: 'Nature Boy',
      youtube: {
        link: 'https://youtu.be/LNpwBpZUrzk',
        description: 'Pomplamoose (as a ballad)'
      },
      key: 'Fm',
      style: 'Bossa',
      source: 'iReal Pro'
    },
    {
      name: 'Summertime',
      youtube: {
        link: 'https://www.youtube.com/watch?v=gSqe-oi0478',
        description: 'Renee Olstead'
      },
      key: 'Am',
      source: 'iReal Pro'
    },
    {
      name: "Here's That Rainy Day",
      youtube: {
        link: 'https://www.youtube.com/watch?v=owhkKIAbLi4&t=66s',
        description: 'Dolores Gray'
      },
      key: 'G',
      source: 'The Real Book, p. 175'
    },
    {
      name: 'God Bless the Child',
      youtube: {},
      source: 'iReal Pro'
    },
    {
      name: 'Cry Me a River',
      youtube: {},
      source: 'iReal Pro',
      key: 'Cm'
    },
    {
      name: 'Let It Snow',
      youtube: {},
      source: 'The Real Christmas Book',
      key: 'F'
    },
    {
      name: 'White Christmas',
      youtube: {},
      source: 'The Real Christmas Book',
      key: 'C'
    },
    {
      name: 'Baby It\'s Cold Outside',
      youtube: {
        link: 'https://youtu.be/6bbuBubZ1yE',
        description: 'Idina Menzel & Michael Bublé'
      },
      source: 'The Real Christmas Book'
    }
  ] }
})

console.log(app)
